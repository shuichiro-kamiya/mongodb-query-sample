var util = require('util');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/myproject';

module.exports =
selectAttrs=function(attrs_array, callback, collectionName) {

    if(attrs_array==null || attrs_array.length<=0) {
        callback(null,null);
    }

    MongoClient.connect(url, function (err, db) {
        // ** テストのために本来のコレクション名を使用していません ***
        // var coll_name = "category"; // コレクション名
        var collection = db.collection(collectionName);

        // 条件の組み立て

        var matchCondition = buildMatchCondition(attrs_array);
        // console.log(util.inspect(matchCondition,false,null));
        // 検索と集計の実行

        collection.aggregate([
            {$match: matchCondition},
            {$unwind: "$attrs"},
            {$group: {_id: "$attrs"}}
        ], function (err, results) { 
            db.close();
            if(err){
                console.log(err);
                callback(err,null);
            }
            else {
                callback(null,results);
            }	
        });
    });
};

function buildMatchCondition(attrs_array) {
    var all = attrs_array.map(function(attr){
        if(attr.$gt && attr.$lt) return buildNumericCondition(attr);
        if(attr.values) return buildArrayCondition(attr);
        return buildStringCondition(attr);
    });
    return {"attrs": { "$all": all }};
}

// 文字列条件の場合は問題なし
function buildStringCondition(attr) {
    return {"$elemMatch": { "key": attr.key, "value":attr.value} };
}

// 数値の範囲条件の場合は現在の実装では条件の入力側から
// どのようにして範囲の始まり($gt=greater than)、終わり($lt=lesser than)を渡すかが問題となる
// この実装ではattr.$gt, attr.$ltにセットされている想定
function buildNumericCondition(attr) {
    return {"$elemMatch": { "key": attr.key, "value": {"$gt": attr.$gt, "$lt": attr.$lt} } };
}

// 配列の条件の場合も同様に条件の入力側から
// どのようにして条件の配列を渡すかが問題になる
// この実装ではattr.valuesにセットされている想定
function buildArrayCondition(attr) {
    return {"$elemMatch": {"key": attr.key, "value": { $all: attr.values} } };
}
