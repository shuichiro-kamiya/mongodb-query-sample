var expect = require('chai').expect;
var Import = require('./util/import');
var selectAttrs = require('../sample');


var caseName;
var caseNo = 1;
beforeEach(function() {
    caseName = 'case'+ caseNo++;
    return Import(caseName);
});



describe('文字列条件',function() {
    it('完全一致する場合のみ抽出される', function(done) {
        var attrs_array = [
            {key: 'カテゴリ', value: '新巻鮭'}
        ];

        selectAttrs(attrs_array, function(err, results) {

            expect(results).to.have.length(1);

            done();
        }, caseName);
    });

    it('複数指定したときはAND検索', function(done) {
        var attrs_array = [
            {key: 'カテゴリ', value: '新巻鮭'},
            {key: 'アレルギー', value: 'さけ'},
        ];

        selectAttrs(attrs_array, function(err, results) {

            expect(results).to.have.length(1);

            done();
        }, caseName);
    });
});

describe('数値範囲条件',function() {
    it('$gt < value < $ltの範囲指定ができる', function(done) {
        var attrs_array = [
            {key: '本体価格', $gt: '3000', $lt: '6000'},
        ];

        selectAttrs(attrs_array, function(err, results) {

            expect(results).to.have.length(2);

            done();
        }, caseName);
    });

    it('複数指定したときはAND検索', function(done) {
        var attrs_array = [
            {key: '本体価格', $gt: '3000', $lt: '6000'},
            {key: '上代', $gt: '101', $lt: '1001'}
        ];

        selectAttrs(attrs_array, function(err, results) {

            expect(results).to.have.length(1);

            done();
        }, caseName);
    });
});

describe.skip('配列条件', function() {
    it('ひとつ指定できる', function(done) {
        done();
    });

    it('複数指定できる', function(done) {
        done();
    });
});

describe.skip('複合条件', function() {
    it('条件をそれぞれひとつずつ指定できる', function(done) {
        done();
    });
    it('条件をそれぞれふたつずつ指定できる', function(done) {
        done();
    });
});
