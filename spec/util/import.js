var path = require('path');
var Promise = require('bluebird');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/myproject';

module.exports = function(caseName) {

    return Drop(caseName).then(function() {
        return Import(caseName);
    });
};


function Import(caseName) {

    return new Promise(function(resolve, reject) {
        MongoClient.connect(url, function (err, db) {
            
            var data = require(path.join(__dirname, '/data/'+caseName));
            var collection = db.collection(caseName);

            collection.insert(data, function (err, results) {
                db.close();
                if(err){
                    console.log(err);
                    reject(err);
                }
                else {
                    resolve(results);
                }
            });
        });
    });
};

function Drop(collectionName) {

    return new Promise(function(resolve, reject) {
        MongoClient.connect(url, function (err, db) {

            var collection = db.collection(collectionName);

            collection.drop(function (err, reply) {
                db.close();
                resolve(reply);
            });
        });
    });
}
